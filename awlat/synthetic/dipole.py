# Standard library imports
from math import cos, sin, sqrt

# Third party imports
import numpy as np

# Internal Package imports
import awlat as aw
from awlat.signal import concatenate
from awlat.shots.factories import shot_monopole_from_x_and_function
from awlat import settings


def reflection_shot_P_wave(  # TODO Test...
    source_wave: aw.signal.NumpySignal,
    Z: float,
    alpha: float,
    v: float,
    reflection_angle: float,
    tool: aw.AcousticTool,
    direct_wave: 'NumpySignal containing the direc_wave to be added' = None
) -> aw.shots.ShotDipole:
    """
    Simple simulation of a dipole reflection on a inclined plane.

    :param source_wave: waveform to be used as the base signal
    :param Z: source distance to bed intersection with the well
    :param alpha: angle of the bed with the vertical
    :param v:  wave velocity in the rock
    :param reflection_angle: azimutal plane's reflection angle
    :param tool: tool
    :param direct_wave: direct wave to be added
    """
    # Hardcoding stuff to simplify #FIXME
    reflection_coefficient = 1
    #
    max_delay = 0
    components = []
    for z in tool.receivers_positions:
        atime = reflection_arrival_time(z, Z, alpha, v)
        # storing the greatest arrival time to build a common x axis latter.
        if atime > max_delay:
            max_delay = atime
        delayed_signal = source_wave.add_delay(atime)
        components_receiver_z = dipole_reflection_components_P_wave(
            delayed_signal,
            reflection_coefficient,
            reflection_angle,
            tool)
        components.append(components_receiver_z)
    # making all signals match a single x sample time (adding zeros and
    # sampling).
    common_x = np.arange(
        source_wave.x[0], source_wave.x[-1]+max_delay, source_wave.dx)
    XX, XY, YX, YY = [], [], [], []
    for components_receiver_z in components:
        CC_signal_common_x = \
            [shot_monopole_from_x_and_function(
                common_x,
                components_receiver_z[i].extended_domain())
                for i in range(len(components_receiver_z))]
        XX.append(CC_signal_common_x[0])
        XY.append(CC_signal_common_x[1])
        YX.append(CC_signal_common_x[2])
        YY.append(CC_signal_common_x[3])
    # Merging signals
    XX = concatenate(XX)
    XY = concatenate(XY)
    YX = concatenate(YX)
    YY = concatenate(YY)
    # Adding the direct wave if
    if direct_wave is not None:
        # Transforming the direct_wave to the common_x sampling +
        # extending.
        direct_wave_compatible = [shot_monopole_from_x_and_function(
            common_x, direct_wave.select_y(i).extended_domain())
            for i in range(direct_wave.y_len)]
        direct_wave_compatible = concatenate(direct_wave_compatible)
        #
        XX = XX + direct_wave_compatible
        XY = XX + direct_wave_compatible
        YX = XX + direct_wave_compatible
        YY = XX + direct_wave_compatible
    return settings.ShotDipole(XX, XY, YX, YY)


def dipole_reflection_components_P_wave(
    source_wave: 'NumpySignal',
    reflection_coefficient: 'float',
    reflection_angle: 'float',
    tool: 'float'
) -> 'XX,XY,YX,YY : 4 * NumpySignal':
    """
        Returns the XX, XY, YX and YY data from a source wave using a
        very simple model taken from 'Imaging near-borehole structure
        using directional acoustic-wave measurement', Xiaoming Tang.
    """
    # TODO - test if angle is with the right sign.
    reflected_wave = source_wave * reflection_coefficient
    XX = reflected_wave * cos(reflection_angle)**2
    XY = reflected_wave * sin(reflection_angle)*cos(reflection_angle)
    YX = reflected_wave * sin(reflection_angle)*cos(reflection_angle)
    YY = reflected_wave * sin(reflection_angle)**2
    return XX, XY, YX, YY


def reflection_arrival_time(
        z: 'float - receiver distance to source',
        Z: 'float - source distance to bed intersection with the well',
        alpha: 'angle of the bed with the vertical',
        v: 'float - wave velocity in the rock'
) -> 'float - arrival wave time':
    """
        Computes the arival time for a wave reflecting on a plane.
        Based on (including symbols notation) "Processing array
        acoustic-logging data to image near-borehole geologic
        structures" Tang et al.
    """
    if Z > 0 and (Z-z) < 0:
        # this condition true implies the receiver is above the bad but
        # the source below, resulting in no reflection.
        print('Source below bed but receiver on top.. no reflection. \
            Returning None.')
        return None
    else:
        return sqrt(z**2 + 4*Z*(Z-z)*sin(alpha)**2)/v


def reflection_shot_S_wave(
    source_wave: 'NumpySignal containing the waveform - just the first y will '
        'be used.',
    Z: 'float - source distance to bed intersection with the well',
    alpha: 'angle of the bed with the vertical',
    v: 'float - wave velocity in the rock',
    reflection_angle: "float - azimutal plane's reflection angle",
    transfer_functions: "(float,float) with the Tsh and Tsv components "
        "transfer functions",
    tool: 'AcousticTool',
    direct_wave: 'NumpySignal containing the direc_wave to be added' = None
) -> 'DipoleShot':
    """
    Simple simulation of a dipole S-waves reflection on a inclined plane.
    """
    max_delay = 0
    components = []
    for z in tool.receivers_positions:
        atime = reflection_arrival_time(z, Z, alpha, v)
        # storing the greatest arrival time to build a common x axis latter.
        if atime > max_delay:
            max_delay = atime
        delayed_signal = (source_wave.select_y(0) 
            .add_padding(int(atime/source_wave.dx)) 
            .propagate_wave([v*atime], v))
        # NOTE: As there is no dependency on theta implemented (dumb
        # transfer functions), there is no dependency on z, therefore
        # this components_receiver_z will all be the same.
        # But I'll leave it here just in case as this is not a speed
        # critical code part.
        components_receiver_z = dipole_reflection_components_S_wave(
            reflection_angle,
            transfer_functions,
        )
        components.append(
            [delayed_signal * comp for comp in components_receiver_z])
    # making all signals match a single x sample time (adding zeros and
    # sampling).
    common_x = np.arange(
        source_wave.x[0], source_wave.x[-1]+max_delay, source_wave.dx)
    #common_x = source_wave.x[:]
    XX, XY, YX, YY = [], [], [], []
    for components_receiver_z in components:
        CC_signal_common_x = [shot_monopole_from_x_and_function(
            common_x, components_receiver_z[i].extended_domain())
            for i in range(len(components_receiver_z))]
        XX.append(CC_signal_common_x[0])
        XY.append(CC_signal_common_x[1])
        YX.append(CC_signal_common_x[2])
        YY.append(CC_signal_common_x[3])
    # Merging signals
    XX = concatenate(XX)
    XY = concatenate(XY)
    YX = concatenate(YX)
    YY = concatenate(YY)
    # Adding the direct wave if
    if direct_wave is not None:
        # Transforming the direct_wave to the common_x sampling +
        # extending.
        direct_wave_compatible = [shot_monopole_from_x_and_function(
            common_x, direct_wave.select_y(i).extended_domain())
            for i in range(direct_wave.y_len)]
        direct_wave_compatible = concatenate(direct_wave_compatible)
        #
        XX = XX + direct_wave_compatible
        XY = XY + direct_wave_compatible
        YX = YX + direct_wave_compatible
        YY = YY + direct_wave_compatible
    return settings.ShotDipole(XX, XY, YX, YY)


def dipole_reflection_components_S_wave(
    reflection_angle: 'float',
    transfer_functions: "(float,float) with the Tsh and Tsv components "
       "transfer functions",
) -> ' XX,XY,YX,YY : 4 * float':
    """
    Returns the XX, XY, YX and YY coefficients from the S-wave
    reflections from a source using a very simple model taken from
    'Single-well S-wave imaging using multicomponent dipole acoustic-log
    data' - X. Tang and D. J. Patterson.
    Angle based on the "source at bottom" tool.
    """
    Th = transfer_functions[0]
    Tv = transfer_functions[1]
    #
    XX = Th*cos(reflection_angle)**2 + Tv*sin(reflection_angle)**2
    XY = -(Tv-Th) * sin(reflection_angle)*cos(reflection_angle)
    YX = -(Tv-Th) * sin(reflection_angle)*cos(reflection_angle)
    YY = Th*sin(reflection_angle)**2 + Tv*cos(reflection_angle)**2
    return XX, XY, YX, YY
