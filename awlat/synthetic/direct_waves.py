# Standard library imports
from cmath import exp
from math import pi

# Third party imports
import numpy as np

# Internal Package imports
from awlat.signal import NumpySignal


def gen_direct_waves(waveform, d, N, v):
    """
    Generates a synthetic direct wave signal in N receptor taking into
    account the dispersion characteristis present in v.
    waveform: NumpySignal-like : first signal ([0]) will be used as a
    reference.
    v : NumpySignal or constant : dispersion curve.
    """
    # Taking just the first waveform as a reference.
    wave_reference = NumpySignal(waveform.x, waveform.y[0])
    wave_reference_fft = wave_reference.fft()
    #
    ys = []
    for i, freq in enumerate(wave_reference_fft.x):
        new_y = [wave_reference_fft.y[0][i]]
        # Adapting for fixed velocity or dispersion curve.
        try:
            vf = v(freq)
        except:
            vf = v
        # Precomputing the most part of the exponential factor.
        factor = -1j*2*pi*freq/vf
        for n in range(N)[1:]:
            new_y.append(wave_reference_fft.y[0][i]*exp(factor*n*d))
        ys.append(new_y)
    ys = np.array(ys).transpose()
    return waveform.new(wave_reference_fft.x, ys).fft_inverse()
