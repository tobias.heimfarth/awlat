class AcousticTool:
    def __init__(self,
                 npole,
                 diameter,
                 number_of_receivers,
                 receivers_spacing,
                 first_receiver_distance_from_source):
        self._receivers_positions = [first_receiver_distance_from_source
                                     + i*receivers_spacing for i in 
                                     range(number_of_receivers)]
        self.number_of_receivers = number_of_receivers
        self.receivers_spacing = receivers_spacing
        self.diameter = diameter
        self.npole = npole

    @property
    def receivers_positions(self):
        return self._receivers_positions

    @receivers_positions.setter
    def receivers_positions(self, pos):
        if type(pos) != list:
            print('Error. The positions must be given in a list.')
        else:
            self._receivers_positions = pos

    def receivers_midpoints(self):
        '''
        Returns a dictionary with the all possible receiveres pairs (by
        indexes) and their midpoints.
        '''
        midpoints = {}
        for n1, z1 in enumerate(self.receivers_positions[:-1]):
            for _n2, z2 in enumerate(self.receivers_positions[n1+1:]):
                n2 = _n2 + 1
                delta_z = z2 - z1
                midpoints.update({(n1, n2): delta_z})
        return midpoints

