"""
Usefull general purpose bits and pieces.
"""
from random import random
from math import exp as exp_real


def minimize_simulated_annealing(initial_parameters,
                                 energy_function,
                                 temperature,
                                 max_times,
                                 cooling_rate=0.05,
                                 guided=True):
    """
    Algorithm to minimize a given energy by randomly starting to walk
    in a certain direction and keep going until the energy stops to
    decrease.
    """
    K = 0.005
    parameters = initial_parameters
    E = energy_function(**parameters)
    # Storing the best yet.
    best_pars = parameters
    best_E = E
    #
    improve = False
    # Main looppars_fitted[par]
    for _ in range(max_times):
        # If not improved randomize the step direction
        if not improve or not guided:
            test_parameters_delta = {key: value*temperature*(0.5-random()) for
                                     key, value in parameters.items()}
        # Sums the step to the absolute parameter vector.
        test_parameters = {key: value + test_parameters_delta[key] for key,
                           value in parameters.items()}
        # Computing the candidate Energy.
        test_E = energy_function(**test_parameters)
        # Store if it is the best.
        if test_E < best_E:
            best_pars = test_parameters
            best_E = test_E
        # Jump?
        delta_E = test_E - E
        if delta_E < 0:
            parameters = test_parameters
            E = test_E
            improve = True
        else:
            if random() < exp_real(-delta_E/(K*temperature)):
                parameters = test_parameters
                E = test_E
            improve = False
            temperature = temperature*(1-cooling_rate)
        # print(E,test_E,parameters)
        # if E < E_treshold:
        #    print('Encontrou minimo')
        #    break
    return best_pars
