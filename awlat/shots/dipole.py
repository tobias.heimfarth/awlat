import os.path
from math import sqrt, cos, sin, atan, pi

import matplotlib.pyplot as plt
import numpy as np

from awlat.signal.numpy_signal import NumpySignal


class ShotDipole:
    """ 
    Stores and manipulate the temporal data from a single depth
    The XY notation is such that X refers to the source orientation
    and Y to the receiver.
    """
    @classmethod
    def new(cls, *args):
        """
        Creates a new instance of ShotDipole or a child class.
        To be used in the "not in-place" methods to ensure that the
        result is from the same class as the object used to create it.
        """
        return cls(*args)

    def __init__(self, XX, XY, YX, YY):
        self.XX = XX
        self.XY = XY
        self.YX = YX
        self.YY = YY
        self._fft = None

    @property
    def components(self):
        """
        Allow to get the componets in a list.
        """
        return [self.XX, self.XY, self.YX, self.YY]

    @components.setter
    def components(self, comps):
        """
        Allow setting the components by a list.
        """
        self.XX = comps[0]
        self.XY = comps[1]
        self.YX = comps[2]
        self.YY = comps[3]

    def __getattr__(self, name):
        """
        Runs a method from XX,XY... class (NumpySignal or children) for
        all components.
        """
        def wrapper(*args, **kwargs):
            results = []
            for component in self.components:
                method = getattr(component, name)
                results.append(method(*args, **kwargs))
            # Returns a new instance of ShotDipole if the method returns
            # a new instance of self.XX, ...
            if [isinstance(results[i], type(self.components[i]))
                    for i in range(len(results))] == [True]*4:
                return self.new(*results)
            else:
                return results
        return wrapper

    @property
    def fft(self):
        """
        Computes and stores the fft from the components.
        Do not use the __getattr__ automatic method to avoid
        creating a separeted ShotDipole instance cotaining just the 
        ffts.
        """
        if self._fft is None:
            self._fft = [comp.fft() for comp in self.components]
        return self._fft

    def __getstate__(self):
        """
        Manually passing the instance state to avoid pickling erros
        in the multiprocess calls.
        FIXME : Maybe there is a more appropriate multiprocess
        way of calling this functions that do not require this method.
        """
        self_dict = self.__dict__.copy()
        #del self_dict['pool']
        return self_dict

    def __setstate__(self, d):
        """
        Complement to __getstate__.
        """
        self.__dict__ = d

    def save_txt(self, filename):
        """
        Quick save in 4 different files.
        """
        name, ext = os.path.splitext(filename)
        self.XX.savetxt("{name}_{uid}{ext}".format(
            name=name, uid='XX', ext=ext))
        self.YX.savetxt("{name}_{uid}{ext}".format(
            name=name, uid='YX', ext=ext))
        self.XY.savetxt("{name}_{uid}{ext}".format(
            name=name, uid='XY', ext=ext))
        self.YY.savetxt("{name}_{uid}{ext}".format(
            name=name, uid='YY', ext=ext))


def rotate_components_matrix(sd, angle):
    """
    Same as rotate_components but using matrix multiplication.
    """
    # Pre computing trigonometric functions.
    cos_angle = cos(angle)
    sin_angle = sin(angle)
    # Stacking up the matrizes.
    M1 = np.array([[cos_angle, sin_angle], [-sin_angle, cos_angle]])
    M2 = np.array([[cos_angle, -sin_angle], [sin_angle, cos_angle]])
    MCC = np.array([[sd.XX, sd.XY], [sd.YX, sd.YY]])
    #
    MCC_rot = np.matmul(np.matmul(M1, MCC), M2)
    return sd.new(MCC_rot[0, 0],
                  MCC_rot[0, 1],
                  MCC_rot[1, 0],
                  MCC_rot[1, 1])
