# Standard library imports
from math import sqrt, cos, sin, pi
from cmath import exp

# Third party imports
import matplotlib.pyplot as plt
import numpy as np

# Internal Package imports
from awlat.signal import NumpySignal


def plot_shot_monopole(nps,
                       show=False,
                       title='',
                       xlabel='',
                       ylabel='',
                       ax=None,
                       **kargs):
    """
    Plot multiple signals in a multiple axis format.
    Avoids the stacking provided by the standard NumpySignal plot
    method.
    """
    N = nps.y_len
    # Discarting imaginary part.
    nps_real = nps.real()
    # Creating figure and axes for new plot if no axes was given.
    if ax is None:
        fig, ax = plt.subplots(N, 1, sharex=True, sharey=True)
        # Making sure axs is an array.
        if np.array(ax).shape == ():
            ax = np.array([ax])
        # Remove horizontal space between axes
        fig.subplots_adjust(hspace=0)
        # Renaming axis.
        fig.suptitle(title)
        ax[N-1].set_xlabel(xlabel)
        ax[int((N-1)/2)].set_ylabel(ylabel)
    else:
        # Getting figure from first axes.
        try:
            fig = ax[0].figure
        except TypeError:
            fig = ax.figure
    # Plotting...
    for i, y in enumerate(nps_real.y):
        ax[i].plot(nps_real.x, y, **kargs)
    # Shows if asked.
    if show:
        plt.show()
    return fig, ax


class ShotMonopole(NumpySignal):
    """
    Child of NumpySignal with some special methods for
    the monopole analisis.
    """

    def __getstate__(self):
        """
        FIXME if possible. Work around the future.result calling
        this method.
        """
        self_dict = self.__dict__.copy()
        #del self_dict['pool']
        return self_dict

    def __setstate__(self, d):
        self.__dict__ = d

    plot = plot_shot_monopole

# Factories


