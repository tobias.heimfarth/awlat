import os.path

import numpy as np

from awlat import settings


def loadtxt_shot_monopole(filename, *args, **kwargs):
    xy = np.loadtxt(filename, *args, **kwargs).transpose()
    return settings.ShotMonopole(xy[0], xy[1:])


def loadz_shot_monopole(filename, *args, **kwargs):
    npzfile = np.load(filename, *args, **kwargs)
    return settings.ShotMonopole(npzfile['x'], npzfile['y'])


def load_shot_monopole(filename, *args, **kwargs):
    """
    Redirects the file loading function by file extension.
    """
    fileName, fileExtension = os.path.splitext(filename)
    if fileExtension == '.npz' or fileExtension == 'npz':
        return loadz_shot_monopole(filename, *args, **kwargs)
    else:
        return loadtxt_shot_monopole(filename, *args, **kwargs)


def shot_monopole_from_x_and_function(x, f):
    """
    Creates a ShotMonopole instance from a x dataset and a function
    f(x).
    """
    y = []
    for xi in x:
        y.append(f(xi))
    return settings.ShotMonopole(x, y)