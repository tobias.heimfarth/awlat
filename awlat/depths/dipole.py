import matplotlib.pyplot as plt
import numpy as np

from awlat.spectral_semblance import semblance_vectorized
from awlat.signal import extended_domain



class DipoleComponents:
    """
    Basic structure to handle quantities with multiple components
    mirroring the dipole signal components (XX,XY,YX,YY).
    """

    def __init__(self):
        self.components = [None]*4

    @property
    def XX(self): return self.components[0]
    @XX.setter
    def XX(self, value): self.components[0] = value

    @property
    def XY(self): return self.components[1]
    @XY.setter
    def XY(self, value): self.components[1] = value

    @property
    def YX(self): return self.components[2]
    @YX.setter
    def YX(self, value): self.components[2] = value

    @property
    def YY(self): return self.components[3]
    @YY.setter
    def YY(self, value): self.components[3] = value

    def __getitem__(self, item):
        return self.components[item]

    def __setitem__(self, item, value):
        self.components[item] = value

    def __iter__(self):
        self.n = 0
        return self

    def __next__(self):
        if self.n < 4:
            self.n += 1
            return self.components[self.n-1]
        else:
            raise StopIteration


class DepthDipole:
    """
    Basic class to deal with dipoles depths.
    Defines de methods to be called by the WellLog class when running a
    well.
    """

    def __init__(self, shot, tool, **k_pars):
        # Variables called externally (required by WellLog)
        self.pars = {'depth': None,
                     'n': tool.npole}
        self.pars.update(k_pars)
        # Internal variables
        self.tool = tool
        self.shot = shot

    @classmethod
    def new(cls, x, *args, **kwargs):
        """
        Creates a new instance of DepthDipole or a child class.
        To be used in the "not in-place" methods to ensure that the
        result is from the same class as the object used to create it.
        """
        return cls(x, *args, **kwargs)

    def __getstate__(self):
        """
        Manually passing the instance state to avoid pickling erros
        in the multiprocess calls.
        FIXME : Maybe there is a more appropriate multiprocess
        way of calling this functions that do not require this method.
        """
        return self.__dict__

    def __setstate__(self, d):
        """
        Complement to __getstate__.
        """
        self.__dict__ = d

    def __getattr__(self, attr, *args, **kwargs):
        # First looks for the attribute to be in the pars list.
        try:
            return self.pars[attr]
        except KeyError:
            pass
        # Second searches for methods in the shot
        try:
            def wrapper(*args, **kwargs):
                method = getattr(self.shot, attr)
                result = method(*args, **kwargs)
                if isinstance(result, type(self.shot)):
                    return self.new(result, self.tool, **self.pars)
                else:
                    return result
            return wrapper
        except AttributeError:
            print(f'{attr} attribute does not exist.')
            raise AttributeError(f'{self.__class__.__name__}.{attr} \
                is invalid.')

    def update(self, *args, **kwargs):
        """
        Method to update self.pars that can be used by WellLog
        for looping over all depths.
        """
        self.pars.update(*args, **kwargs)

    def pop(self, *args, **kwargs):
        """
        Method to pop self.pars that can be used by WellLog
        for looping over all depths.
        """
        self.pars.pop(*args, **kwargs)


class DepthDipoleSemblance(DepthDipole):
    """
    Extending the DepthLogDipole class to deal with the spectral 
    semblance.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._spectral_semblance = None

    @property
    def spectral_semblance(self):
        if self._spectral_semblance is None:
            print('No spectral semblance were computed yet, computing with no'
                  'filter and standard parameters.')
            self.get_spectral_semblance()
        return self._spectral_semblance
    
    @spectral_semblance.setter
    def spectral_semblance(self,semblance):
        self._spectral_semblance = semblance

    def get_spectral_semblance(self, ws_sigma=0, store = True, **kwargs):
        _spectral_semblance = DipoleComponents()
        _spectral_semblance.components = \
            [semblance_vectorized(fft, self.tool.receivers_positions, **kwargs)
             for fft in self.shot.fft]
        if ws_sigma != 0:
            _spectral_semblance.components = (
                [spectral_semblance.filter_gaussian(ws_sigma)
                 for spectral_semblance in _spectral_semblance])
        if store:
            self._spectral_semblance = _spectral_semblance
        return _spectral_semblance


