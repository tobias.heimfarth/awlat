from awlat import settings
from awlat.signal import NumpySignal,stack

def stack_by_receiver_fixed_position(depthlogs,common_t = False):
    """
    Returns a "fixed receiver position" stacking scheme.
    common_t = True assumes that all waveforms have the same t axis
    and greatly improves speed.
    FIXME Generalize to monopole.
    FIXME Break this function in pieces for readability.
    """
    tool = depthlogs[0].tool
    N_receivers = tool.number_of_receivers
    N_depths = len(depthlogs)
    spacing = tool.receivers_spacing
    new_shot = depthlogs[0].shot.new
    # Tool number of steps per spacing - #FIXME assuming and integer.
    m = int(spacing/(depthlogs[1].depth - depthlogs[0].depth))
    depthlogs_stacked = []
    if common_t:
        t_axis = depthlogs[0].shot.XX.x
        for i in range(N_depths-(N_receivers-1)*m):
            XX, XY, YX, YY = [], [], [], []
            for j in range(N_receivers):
                components = [component.y[j*m]
                    for component in depthlogs[i+j*m].shot.components]
                XX.append(components[0])
                XY.append(components[1])
                YX.append(components[2])
                YY.append(components[3])
            XX = NumpySignal(t_axis,XX)
            XY = NumpySignal(t_axis,XY)
            YX = NumpySignal(t_axis,YX)
            YY = NumpySignal(t_axis,YY)
            new_depthlog = depthlogs[0].new(settings.ShotDipole(XX, XY, YX, YY),
                                            tool)
            # using the middle shot to get the parameters (middle
            # detector as reference)
            ref = i + int(N_receivers/2)
            new_depthlog.pars = depthlogs[ref].pars
            depthlogs_stacked.append(new_depthlog)
    else:
        for i in range(N_depths-(N_receivers-1)*m):
            XX, XY, YX, YY = [], [], [], []
            for j in range(N_receivers):
                components = [component.select_y(
                    j*m) for component in depthlogs[i+j*m].shot.components]
                XX.append(components[0])
                XY.append(components[1])
                YX.append(components[2])
                YY.append(components[3])
            XX = stack(XX, XX[0].x)
            XY = stack(XY, XY[0].x)
            YX = stack(YX, YX[0].x)
            YY = stack(YY, YY[0].x)
            new_depthlog = depthlogs[0].new(settings.ShotDipole(XX, XY, YX, YY),
                                            tool)
            # using the middle shot to get the parameters (middle
            # detector as reference)
            ref = i + int(N_receivers/2)
            new_depthlog.pars = depthlogs[ref].pars
            depthlogs_stacked.append(new_depthlog)
    return depthlogs_stacked
