# Standard library imports

# Third party imports
import matplotlib.pyplot as plt

# Internal Package imports
from awlat.spectral_semblance import semblance_vectorized


class DepthMonopole:
    """
    Template for the depth-like classes to work with the WellLog class.
    Maybe further down the line use the ABC (Abstract Base Classes),
    leaving to the "user" to make sure all the methods works properly
    for now.
    """

    def __init__(self, signal, tool, **k_pars):
        self.signal = signal
        self.pars = {'depth': None,
                     'r_well': None,
                     'alpha': None,
                     'beta': None,
                     'rho': None,
                     'alpha_f': None,
                     'rho_f': None}
        self.pars.update(k_pars)
        self._spectral_semblance = None
        self._fft = None
        self.tool = tool

    def __getattr__(self, atrib):
        try:
            return self.pars[atrib]
        except:
            print(f'{atrib} attribute does not exist.')
            raise AttributeError

    def spectral_semblance(self, ws_sigma=0):
        if self._spectral_semblance is None:
            self.fft()
            self._spectral_semblance = semblance_vectorized(self._fft,
                                                self.tool.receivers_positions)
            if ws_sigma != 0:
                self._spectral_semblance = (self._spectral_semblance
                                            .filter_gaussian(ws_sigma))
        return self._spectral_semblance

    def fft(self):
        if self._fft is None:
            self._fft = self.signal.fft()
        return self._fft
