from awlat.shots.monopole import ShotMonopole
from awlat.shots.dipole import ShotDipole
from awlat.depths.monopole import DepthMonopole
from awlat.depths.dipole import DepthDipoleSemblance

class Settings:
    """
    Store the global values and classes to be used all around.
    Be sure to use compatible shots and depths.
    """
    def __init__(self):
        self.ShotDipole = None
        self.ShotMonopole = None
        self.DepthMonopole = None
        self.DepthDipole = None


settings = Settings()
settings.ShotMonopole = ShotMonopole
settings.ShotDipole = ShotDipole
settings.DepthMonopole = DepthMonopole
settings.DepthDipole = DepthDipoleSemblance