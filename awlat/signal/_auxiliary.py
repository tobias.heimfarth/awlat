from scipy.signal import butter, lfilter


def find_closest_value_index(values, reference):
    """
    Simple function to get the index of the closest value to a reference
    in a given list.
    """
    if not values:
        return None
    diffs = [abs(v - reference) for v in values]
    d_min = min(diffs)
    return diffs.index(d_min)

# Filtering

def get_butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def filter_butter_bandpass(data, lowcut, highcut, fs, order=5):
    b, a = get_butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y


def get_butter_highpass(highcut, fs, order=5):
    nyq = 0.5 * fs
    high = highcut / nyq
    b, a = butter(order, high, btype='highpass')
    return b, a


def filter_butter_highpass(data, highcut, fs, order=5):
    b, a = get_butter_highpass(highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y
