"""
Functions to build a NumpySignal from various starting points.
"""
import numpy as np
from .numpy_signal import NumpySignal
import os.path


def loadz(filename, *args, **kwargs):
    npzfile = np.load(filename, *args, **kwargs)
    return NumpySignal(npzfile['x'], npzfile['y'])


def loadtxt(filename, *args, **kwargs):
    xy = np.loadtxt(filename, *args, **kwargs).transpose()
    return NumpySignal(xy[0], xy[1:])


def load(filename, *args, **kwargs):
    """
    Redirects the file loading function by file extension.
    """
    fileName, fileExtension = os.path.splitext(filename)
    if fileExtension == '.npz' or fileExtension == 'npz':
        return loadz(filename, *args, **kwargs)
    else:
        return loadtxt(filename, *args, **kwargs)


def NumpySignalFromdx(dx, y, x0=0):
    y = np.array(y)
    x = np.arange(y.size)*dx + x0
    return NumpySignal(x, y)


def signal_from_function(f, x0, xf, N):
    """
    Warning: not tested!!
    Creates a NumpySignal instance from a function f(x),
    endpoints and number of points.
    """
    if N < 1:
        print('N must be greater than 1.')
    x = np.linspace(x0, xf, N)
    y = []
    for xi in x:
        y.append(f(xi))
    return NumpySignal(x, y)


def signal_from_x_and_function(x, f):
    """
    Creates a NumpySignal instance from a x dataset and a function f(x).
    """
    y = []
    for xi in x:
        y.append(f(xi))
    return NumpySignal(x, y)
