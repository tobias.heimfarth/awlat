# Standard library imports
from cmath import exp, pi
from math import sqrt

# Third party imports
import numpy as np
from scipy.optimize import curve_fit as scipy_curve_fit
from scipy.signal import welch
from scipy.integrate import trapz, cumtrapz
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt

# Internal Package imports
from ._auxiliary import find_closest_value_index,\
    filter_butter_bandpass, filter_butter_highpass


def all_ys(nps, function):
    """
    Simple decorator to apply a function to all NumpySignal-like objects
    "ys".
    """
    new_ys = []
    for y in nps.y:
        new_ys.append(function(y))
    return nps.new(nps.x, new_ys)


def fft(nps):
    """
    Computes the Fast Fourier Transform from the signal.
        See numpy.fft for details.
        1/N foward normalized
        One-sided -> fft(k)+fft(-k)
    """
    N = nps.length
    k_max = int(N/2) + 1
    fft = np.fft.fft(nps.y)
    # selecting positive frequencies. Obs.: leaves the nyquist frequency
    # out (good because the normalization changes with the parity of N).
    fft = fft[:, 0:k_max]
    # Normalizing and compensating for the negative frequency
    # counterpart.
    fft = fft*(2/N)
    # 0 frequency don't have negative counterpart.
    fft[:, 0] = fft[:, 0]/2.
    # Computing the x axis (frequencies).
    freq = np.array([i*1/(N*nps.dx) for i in range(k_max)])
    #freq = np.fft.fftfreq(nps.x.size,nps.dx)[0:k_max]
    return nps.new(freq, fft)


def fft_inverse(nps):
    """
    Computes the Inverse Fast Fourier Transform from the signal.
        See numpy.fft for details.
        1/N foward normalized
        One-sided -> fft(k)+fft(-k)
    """
    # Recovering the complex conjugated mirror part of the signal
    # discarted in the forward fft.
    # The division by 2 is also required to compesate for the doubling
    # in the forward fft.
    nps_hs = make_hermitian_symmetric(nps)/2
    N = nps_hs.length
    ifft = np.fft.ifft(nps_hs.y)*N
    tcoord = np.array([i*1/(N*nps_hs.dx) for i in range(N)])
    # tcoord = np.fft.fftfreq(nps_hs.x.size,nps_hs.dx) # Computing the x
    # axis (frequencies).
    return nps.new(tcoord, ifft)


def fft_manual(nps):
    """
    FFT directly from definition, no black box.
    Slow but serves as a reference to faster methods.
    The chosen  normalization is to 
        divide by N the forward and by 1 the reverse.
        This leads to the fft of a pure sine wave to have 
        the same value at the sine frequency as its amplitude.
        in the time domain.
    """
    N = nps.length
    dt = nps.dx
    # Half length is used because the other half is the mirrored
    # complex conjugate and not reflect the signal spectrum (DFT
    # artifact). No N parity distinctions is made to avoid
    # complications. So normalization may be a tiny (depens on N) off
    # for N-odd signals.
    ys = []
    f = []
    k_max = int(N/2)+1
    for k in range(k_max):
        ys_line = []
        for y in nps.y:
            par = 0
            for n, v in enumerate(y):
                par = par + v*exp(-2.*1j*pi*k*n/N)
            ys_line.append(par)
        f.append(k/(N*dt))
        ys.append(ys_line)
    ys = np.array(ys).transpose()*(2/N)
    return nps.new(f, ys)


def make_hermitian_symmetric(nps):
    """
    Returns an Hermitian symmetric version of the signal, that is,
    y[N-m] = (y[m])* .
    A complex conjugate mirrored around the x=0 point is added
    leaving out y[-1].
    """
    N = nps.length
    mirrored_y = np.flip(nps.y, axis=1)[:, 1:-1]
    mirrored_y = np.conj(mirrored_y)
    mirrored_x = np.array([-i*nps.dx for i in range(N)[1:-1]])
    mirrored_x = np.flip(mirrored_x)
    # Stacking the signals
    new_y = np.concatenate((nps.y, mirrored_y), axis=1)
    new_x = np.concatenate((nps.x, mirrored_x))
    return nps.new(new_x, new_y)


def fft_inverse_manual(nps, full_length=True):
    # Recovering the complex conjugated mirror part of the signal
    # discarted in the forward fft.
    # The division by 2 is also required to compesate for the doubling
    # in the forward fft.
    nps_hs = make_hermitian_symmetric(nps)/2
    df = nps_hs.dx
    N = nps_hs.length
    # Main loop
    ys = []
    t = []
    for n in range(N):
        ys_line = []
        for y in nps_hs.y:
            par = 0
            for k, v in enumerate(y):
                par = par + v*exp(2.*1j*pi*n*k/N)
            ys_line.append(par)
        t.append(n/(N*df))
        ys.append(ys_line)
    ys = np.array(ys).transpose()
    return nps.new(t, ys)


def integral(nps):
    """
    Return the area below the function by interpolating neighbor points.
    """
    r = []
    for y in nps.y:
        r.append(trapz(y, x=nps.x))
    r = np.array(r)
    return r


def integral_cumulative(nps):
    """
    Returns the cumulative integral function, 
    that is, the area below the curve up to a given x point.
    """
    ys = cumtrapz(nps.y, dx=nps.dx, axis=1)
    # inserts a 0 as the first point tomaintain the signal length
    ys = np.insert(ys, 0, 0, axis=1)
    return nps.new(nps.x, ys)


def integral_cumulative2(nps):
    """
    Alternative method to integral_cumulative.
    """
    dx = nps.dx
    ys = []
    for y in nps.y:
        y_parcial = 0
        yi = []
        for i in range(len(nps.x)):
            y_parcial = y_parcial + y[i]
            yi.append(y_parcial*dx)
        ys.append(yi)
    # inserts a 0 as the first point to maintain the signal length
    #ys = np.insert(ys,0,0,axis=1)
    return nps.new(nps.x, ys)


def cut(nps, xi, xf):
    """
    Returns a segment of the original function.
    """
    y = nps.y[:, nps.x >= xi]
    x = nps.x[nps.x >= xi]
    y = y[:, x < xf]
    x = x[x < xf]
    return nps.new(x, y)


def fit_linear(nps):
    r = []
    for y in nps.y:
        r.append(np.polyfit(nps.x, y, 1))
    return r


def mean(nps):
    return np.mean(nps.y, axis=1)


def set_mean_null(nps):
    mean = np.reshape(nps.mean(), (-1, 1))
    return nps.new(nps.x, nps.y-mean)


def select_y(nps, ys):
    """
    Returns a NumpySignal with only the selected y indexes (list input).
    """
    if type(ys) == int:
        ys = [ys]
    y = []
    for i in ys:
        y.append(nps.y[i])
    y = np.array(y)
    return nps.new(nps.x, y)


def sort_by_x(nps):
    """
    Sorts the signal based on the x axis.
    """
    old_x = nps.x.tolist()
    old_y = nps.y.transpose().tolist()
    sort_x = sorted(enumerate(old_x), key=lambda x: x[1])
    new_x = []
    new_y = []
    for i in sort_x:
        new_x.append(i[1])
        new_y.append(old_y[i[0]])
    new_y = np.array(new_y).transpose()
    return nps.new(new_x, new_y)


def autocorrelation_manual(nps):
    """
    Manually computed autocorrelation.
    """
    N = nps.y.size
    y = nps.y[0]
    r = []
    for k in range(N-1):
        r_par = 0
        for t in range(k+1, N):
            # print t,k
            r_par = r_par + y[t]*y[t-k]
        r.append(r_par)
    r = np.array(r)*(1./N)
    return r


def autocorrelation(nps):
    """
    Autocorrelation using the numpy.correlate function.
    """
    if nps.x[0] != 0:
        print("NumpySignal.autocorrelation: warning, signal \
               must start at x =0.")
    ys = []
    for y in nps.y:
        ac = np.correlate(y, y, mode='full')
        ys.append(ac)
    x = np.append(-nps.x[::-1][:-1], nps.x)
    return nps.new(x, ys)


def psd_autocorrelation(nps):
    ac = nps.autocorrelation()
    fft = ac.fft()
    fft = fft/nps.length/fft.dx
    return fft


def ptp(nps):
    """
    Returns the peak to peak value from the signals.
    """
    ptp = []
    for y in nps.y:
        ptp.append(np.ptp(y))
    return ptp


def fit_curve(nps, f, **kwargs):
    """
    Fits a signal with a given function.
    Uses scipy.optimize.curve_fit. Consult it for more information.
    """
    newnps = []
    parameters = []
    for y in nps.y:
        pars = scipy_curve_fit(f, nps.x, y, **kwargs)
        parameters.append(pars[0].tolist())
        tup = tuple(pars[0].tolist())  # tupling the the parameters
        #print('Parameters: ',tup)
        def g(x): return f(x, *tup)  # parameterized function
        newnps.append(nps.new(nps.x, g(nps.x)))
    return concatenate(newnps), parameters


def join(nps, npsig):
    """
    Joins two signals checking if the x axis matches.
    Returns a new Numpy Signal.
    """
    # Checking if the x axis matches.
    if not np.array_equal(nps.x, npsig.x):
        print("NumpySignal.join: x axis don't match.")
        return None
    # Joining...
    y = np.concatenate((nps.y, npsig.y), axis=0)
    return nps.new(nps.x, y)


def apply_function_y(nps, f):
    """
    Apply the function to all ys.
    Brute force way, maybe numpy provides a better one. Did not checked.
    """
    ys = []
    for y in nps.y:
        f_y = []
        for iy in y:
            f_y.append(f(iy))
        ys.append(f_y)
    return nps.new(nps.x, ys)


def apply_function_x(nps, f):
    """
    Apply the function to x.
    """
    x = f(nps.x)
    return nps.new(x, nps.y)


def apply_function_xy(nps, f):
    """
    Apply the function to all ys, using x as an argument.
    Brute force way, maybe numpy provides a better one. Did not checked.
    """
    ys = []
    for y in nps.y:
        f_y = []
        for i, iy in enumerate(y):
            f_y.append(f(nps.x[i], iy))
        ys.append(f_y)
    return nps.new(nps.x, ys)


def derivative(nps):
    """
    Returns the derivative of the signals
    """
    ys = []
    for y in nps.y:
        ys.append(np.gradient(y, nps.dx))
    return nps.new(nps.x, ys)


def roots(nps):
    """
    Find the function roots
    """
    roots = []
    for y in nps.y:
        y_roots = []
        for i in range(len(nps.x))[1:]:
            y0 = y[i-1]
            y1 = y[i]
            x0 = nps.x[i-1]
            x1 = nps.x[i]
            if (y0 <= 0 and y1 > 0) or (y0 >= 0 and y1 < 0):
                a, b = np.polyfit([x0, x1], [y0, y1], 1)
                y_roots.append(-b/a)
        roots.append(y_roots)
    return roots


def add_padding(nps, n, value=0):
    """
    Adds to the and of the signals a series of n values
    """
    new_x = np.copy(nps.x)
    pad = np.array([nps.x[-1]+i*nps.dx for i in range(n)])
    new_x = np.concatenate((new_x, pad))
    zeros = np.zeros(n)
    new_y = []
    for y in nps.y:
        new_y.append(np.concatenate((y, zeros)))
    return nps.new(new_x, new_y)


def correlation(nps, f):
    """
    Return the correlation between the signal and a given function.
    Warning: factors not verified.
    """
    new_x = np.copy(nps.x)
    new_y = []
    for x in nps.x:
        new_y.append(nps.apply_function_xy(lambda a, y: y*f(a-x)).integral())
    new_y = np.array(new_y).transpose()
    return nps.new(new_x, new_y)


def signal_from_function(sig_class, f, x0, xf, N):
    """
    Warning: not tested!!
    Creates a sig_class instance from a function f(x),
    endpoints and number of points.
    To be used in correlation_normalized.
    """
    if N < 1:
        print('N must be greater than 1.')
    x = np.linspace(x0, xf, N)
    y = []
    for xi in x:
        y.append(f(xi))
    return sig_class(x, y)


def correlation_normalized(nps, f):
    """
    Return the normalized correlation between the signal and a given
    function.
    Warning: factors not verified.
    #TODO modified but not tested.
    """
    new_x = np.copy(nps.x)
    new_y = []
    mean_y = np.mean(nps.y[0])
    sigma_y = np.std(nps.y[0])
    xi = new_x[0]
    xf = new_x[-1]
    N = len(new_x)
    for x in nps.x:
        g = signal_from_function(type(nps), lambda y: f(y-x), xi, xf, N)
        sigma_g = np.std(g.y[0])
        num = 0
        mean_g = np.mean(g.y[0])
        for i in range(N):
            num += (nps.y[0][i]-mean_y)*(g.y[0][i]-mean_g)
        new_y.append(num/(sigma_g*sigma_y))
    new_y = np.array(new_y).transpose()
    new_y = new_y/N
    return nps.new(new_x, new_y)


def local_max_index(nps):
    """
    Return the local maximuns
    """
    max_all_y = []
    for y in nps.y:
        max = []
        temp = []
        for i in list(range(len(y)))[1:-1]:
            if y[i] > y[i-1]:
                temp = [i]
            elif y[i] == y[i-1]:
                if temp == []:
                    pass
                else:
                    temp.append(i)
            elif y[i] < y[i-1]:
                if temp == []:
                    pass
                else:
                    max += temp
                    temp = []
        #max += temp
        max_all_y.append(max)
    return max_all_y


def select_points(nps, condition_x, condition_y, y=0):
    """
    Return a signal only with the points that mach a given condition.
    Warning, supports only one y !! **
    """
    new_x = []
    new_y = []
    for i in range(nps.x.size):
        if condition_x(nps.x[i]) and condition_y(nps.y[y][i]):
            new_x.append(nps.x[i])
            new_y.append(nps.y[y][i])
    return nps.new(new_x, new_y)


def select_points_xy(nps, condition_xy, y=0):
    """
    Return a signal only with the points that mach a given condition.
    Warning, supports only one y !! **
    """
    new_x = []
    new_y = []
    for i in range(nps.x.size):
        if condition_xy(nps.x[i], nps.y[y][i]):
            new_x.append(nps.x[i])
            new_y.append(nps.y[y][i])
    return nps.new(new_x, new_y)


def smooth_gaussian(nps, sigma, cutoff_sigmas=2):
    """
    Smooth the signal by gaussian weigthing
    """
    # Defining the gaussian fuction (needs to be here?)
    def gauss(x, mu, sigma):
        return 1/sqrt(2*pi*sigma**2)*exp(-(x-mu)**2/(2*sigma**2))
    #
    range = sigma*cutoff_sigmas
    new_y = []
    for x in nps.x:
        new_y.append(nps.cut(x-range, x+range)
                     .apply_function_xy(lambda x_, y_: y_*gauss(x_, x, sigma))
                     .integral())
    new_y = np.array(new_y).transpose()
    return nps.new(nps.x, new_y)


def smooth_gaussian_2(nps, sigma, cutoff_sigmas=2):
    """
    Smooth the signal by gaussian weighthing.
    """
    # Defining the gaussian function (needs to be here?)
    def gauss(x, mu, sigma):
        return 1/sqrt(2*pi*sigma**2)*exp(-(x-mu)**2/(2*sigma**2))
    #
    range = sigma*cutoff_sigmas
    new_y = []
    for x in nps.x:
        num = np.array(nps
                       .cut(x-range, x+range)
                       .apply_function_xy(lambda x_, y_: y_*gauss(x_, x, sigma))
                       .integral())
        den = np.array(nps
                       .cut(x-range, x+range)
                       .apply_function_xy(lambda x_, y_: gauss(x_, x, sigma))
                       .integral())
        new_y.append(num/den)
    new_y = np.array(new_y).transpose()
    return nps.new(nps.x, new_y)


def concatenate(nps_list):
    """
    Joins a list of matching NumpySignals.
    """
    ref = nps_list[0]
    ys = [ref.y]
    for nps in nps_list[1:]:
        if np.array_equal(ref.x, nps.x):
            ys.append(nps.y)
        else:
            print("concatenate: x axis don't match.")
    ys = np.concatenate(ys, axis=0)
    return ref.new(ref.x, ys)


def append(nps_list):
    """
    Appends signals in the same y, offseting the x axis.
    Use first 2 x values to determine the dx.
    """
    ref = nps_list.pop(0)
    dx = ref.dx
    y = ref.y
    x = ref.x
    for nps in nps_list:
        y = np.append(y, nps.y, axis=1)
        x = np.append(x, nps.x+(x[-1]+dx), axis=1)
    return nps_list[0].new(x, y)


def merge(nps_list):
    """
    Merge signals in the same y keeping the original x.
    """
    #ref = nps_list.pop(0)
    y = []
    x = []
    for nps in nps_list:
        y.append(nps.y)
        x.append(nps.x)
    ys = np.concatenate(y, axis=1)
    xs = np.concatenate(x, axis=1)
    return nps_list[0].new(xs, ys)


def merge_mean(nps_list):
    """
    Merge signals.
    """
    #ref = nps_list.pop(0)
    x = []
    for nps in nps_list:
        x.append(nps.x)
    x = np.concatenate(x, axis=1)
    x = np.sort(x)
    ys = []
    for i in x:
        xvalues = []
        for nps in nps_list:
            if i >= nps.x[0] and i <= nps.x[-1]:
                xvalues.append(nps(i))
        ys.append(np.mean(xvalues))
    return nps_list[0].new(x, ys)


def equalize_energy(nps, yn=0):
    """
    Make all ys have the same power taking "yn" as the reference.
    """
    power = (nps**2).integral()
    ys = []
    for i in range(nps.y_len):
        ys.append(nps.y[i]*sqrt(power[0]/power[i]))
    return nps.new(nps.x, ys)


def get_power_rms(nps):
    """
    Computes power from the signal.
    """
    total_time = nps.x[-1]-nps.x[0]
    return integral((nps**2)*(1/total_time))


def max_parabola(nps, d, x_max):
    """
    Fits a parabola to the function and return its maximum.
    Usefull to get a local maximum from a low resolution signal.
    """
    sig = cut(nps, x_max-d, x_max+d)
    _, par = fit_curve(sig, lambda x, a, b, c: a*(x-b)**2+c)
    return par[0][1], par[0][2]


def get_closest_maximum(nps, estimated):
    """
    Given an estimation x value to a maximum, tries to find
    the maximum value by fitting a parabola in the region.
    """
    max_args = local_max_index(nps)[0]
    max_ys = []
    max_xs = []
    for i in max_args:
        try:
            max_x, max_y = max_parabola(nps, 3, nps.x[i])
        except:
            max_x, max_y = nps.x[i], nps(nps.x[i])[0]
            # FIXME think about some log collecting arquiteture to
            # enable this kind of warning without polluting the output.
            # print('Could not fit parabola to find maximum, leaving
            # data maximum untouched.',i,max_x,max_y)
        max_ys.append(max_y)
        max_xs.append(max_x)
    max_index = find_closest_value_index(max_xs, estimated)
    if max_index is not None:
        return max_xs[max_index], max_ys[max_index]
    else:
        return None, None


def savez(nps, file_name):
    np.savez(file_name, x=nps.x, y=nps.y)


def savetxt(nps, filename, separator=' ', xformat='{:.5e}', yformat='{:.5e}'):
    savefile = open(filename, 'w')
    for i, x in enumerate(nps.x):
        line = xformat.format(x)
        for y in nps.y:
            line = line + separator + yformat.format(y[i])
        savefile.write(line+'\n')
    savefile.close()


def abs(nps):
    """
    Absolute value of a signal.
    """
    return nps.new(nps.x, np.abs(nps.y))


def filter_butterworth(nps, lowcut, highcut, fs):
    """
    Filter the signal using the butterworth.
    """
    ys = []
    for y in nps.y:
        ys.append(filter_butter_bandpass(y, lowcut, highcut, fs))
    return nps.new(nps.x, ys)


def filter_butterworth_highpass(nps, highcut):
    """
    Filter the signal using the butterworth highpass.
    """
    ys = []
    for y in nps.y:
        ys.append(filter_butter_highpass(y, highcut, 1/nps.dx))
    return nps.new(nps.x, ys)


def real(nps):
    """
    Returns only the real part of the signal.
    """
    return nps.new(nps.x, nps.y.real)


def imag(nps):
    """
    Returns only the imaginary part of the signal.
    """
    return nps.new(nps.x, nps.y.imag)


def extended_domain(nps, yn=0):
    """
    Returns a function defined for all real values based on the yn
    signal.
    Usefull when the signal have assimptotes in both extremities.
    In the original signal domain nothing is changed.
    From minimum x to -infinity returns first yn value.
    From maximum x to infinity returns the last yn value.
    """
    def nps_extended(x):
        if x < nps.x[0]:
            return nps.y[yn][0]
        elif x > nps.x[-1]:
            return nps.y[yn][-1]
        else:
            return nps(x)[yn]
    return nps_extended


def sample(nps, xs):
    """
    Returns a signal with the values sampled at the xs.
    FIXME: no guardrails.
    """
    ys = []
    for x in xs:
        ys.append(nps(x))
    ys = np.array(ys).transpose()
    return nps.new(xs, ys)


def add_delay(nps, delay):
    """
    Adds a delay to the signal t -> t+delay filling the interval t0 to
    delay with zeros.
    """
    x_delayed = nps.x + delay
    Delta_N = int(delay//nps.dx)
    new_x = [x_delayed[0]-i*nps.dx for i in range(Delta_N)]
    new_x.reverse()
    new_y = [np.zeros(Delta_N) for i in range(nps.y_len)]
    new_x = np.concatenate([new_x, x_delayed], axis=0)
    new_y = np.concatenate([new_y, nps.y], axis=1)
    return nps.new(new_x, new_y)


def propagate_wave(nps, distance, velocity):
    """
    Propagates a signal in distance.
    #TODO Optimize
    """
    fft = nps.fft()
    ys = []
    for i, freq in enumerate(fft.x):
        new_y = []
        # Adapting for fixed velocity or dispersion curve.
        try:
            vf = velocity(freq)
        except:
            vf = velocity
        # Precomputing the most part of the exponential factor.
        factor = -1j*2*pi*freq/vf
        for j in range(nps.y_len):
            new_y.append(fft.y[j][i]*exp(factor*distance[j]))
        ys.append(new_y)
    ys = np.array(ys).transpose()
    return nps.new(fft.x, ys).fft_inverse().real()


def stack(nps_list, x_common):
    """
    Stack multiple signals using the x_common as the x-axis points
    (linear interpolation). If a signal is not defined at a given point,
    than zero is attributed.
    """
    ys = []
    for x in x_common:
        y = []
        for nps in nps_list:
            try:
                y += nps(x)
            except ValueError:
                y += [0]*nps.y_len
        ys.append(y)
    ys = np.array(ys).transpose()
    return nps_list[0].new(x_common, ys)
