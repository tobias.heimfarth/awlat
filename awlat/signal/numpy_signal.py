# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 15:49:10 2013

@author: tobiash
"""
import os.path
import numpy as np
from scipy.optimize import curve_fit
from scipy.signal import welch
from scipy.integrate import trapz, cumtrapz
from scipy.interpolate import interp1d

import matplotlib.pyplot as plt
from cmath import exp, pi
from math import sqrt


def make_sure_is_indexable(x):
    try:
        x[0]
        return x
    except:
        return [x]


class NumpySignal(object):
    def __init__(self, x, *args):
        self.x = np.array(x)
        self.y = []
        for i in args:
            i = make_sure_is_indexable(i)
            if type(i[0]) is list or type(i[0]) is np.ndarray:
                for j in i:
                    self.y.append(j)
            else:
                self.y.append(i)
        self.y = np.array(self.y)
        self.interpolate = None

    @property
    def y_len(self):
        return len(self.y)

    @classmethod
    def new(cls, x, *args):
        """
        Creates a new instance of NumpySignal or a child class.
        To be used in the "not in-place" methods to ensure that the
        result is from the same class as the object used to create it.
        Example: If a is from the A class, a child of NumpySignal,
        then func(a) also is from class A.
        """
        return cls(x, *args)

    @property
    def dx(self):
        # print 'Warning: taking dx from the two first x elements.'
        return self.x[1] - self.x[0]

    @property
    def length(self):
        return self.x.size

    def __call__(self, x):
        """
        Interpolates the signal to return a value in a continuous
        domain.
        """
        # Getting x index for the left neighbor point.
        xi_low = int((x-self.x[0])/self.dx)
        if xi_low < 0 or xi_low >= self.length:
            raise ValueError('Signal not defined at the argument value.')
        # Dealing with the edge case of the last x value.
        if xi_low + 1 == self.length:
            return list(self.y[:, -1])
        # Ratio of dx from the signal to the 
        # dx = (x_input  - x_low neighbor)
        # to be used in the interpolation for all signals.
        dx_ratio = (x - self.x[xi_low])/self.dx
        interpolated_ys = []
        for y in self.y:
            interpolated_ys.append(
                y[xi_low] + (y[xi_low+1]-y[xi_low]) * dx_ratio)
        return interpolated_ys

    def __getitem__(self, key):
        r = []
        for y in self.y:
            r.append(y[key])
        return self.new(self.x[key], r)

    def __add__(self, a):
        if isinstance(a, NumpySignal):
            return self.new(self.x, self.y+a.y)
        else:
            return self.new(self.x, self.y+a)

    def __sub__(self, a):
        if isinstance(a, NumpySignal):
            return self.new(self.x, self.y-a.y)
        else:
            return self.new(self.x, self.y-a)

    def __mul__(self, a):
        if isinstance(a, NumpySignal):
            if np.array_equal(self.x, a.x) and self.y_len == a.y_len:
                return self.new(self.x, self.y*a.y)
            else:
                raise TypeError(
                    "Not maching NumpySignals (x axis must be identical and \
                    the number or y axis be the same).")
        else:
            return self.new(self.x, self.y*a)

    def __rmul__(self, a):
        return self.new(self.x, self.y*a)

    def __truediv__(self, a):
        return self.new(self.x, self.y/a)

    def __pow__(self, pot):
        return self.new(self.x, self.y**pot)

    def all_y(self, f):
        def new(*args, **kwargs):
            r = []
            for y in self.y:
                r = r.append(f(*args, **kwargs))
        return new

    def get_interpolation(self):
        """
        Stores a ready to use interpolation function
        at self.interpolate.
        """
        self.interpolate = []
        for y in self.y:
            self.interpolate.append(interp1d(self.x, y))

    def plot(self, *args, ax=None, show=False, **kwargs):
        """
        Plot the signal using mplotlib.
        Two extra key arguments can be passed:
            fig : append plot to an existing mplotlib figure.
            show : runs fig.show() at the end, needed for standalone
            usage.
        """
        # Looking for existing axis.
        if ax is not None:
            # Making sure ax is not a list (and if it is, discarting
            # others than the 0 axis)
            try:
                ax = ax[0]
            except:
                pass
            fig = ax.figure
        else:
            fig = plt.figure()
            ax = fig.add_subplot(111)
        # Plot the data
        for i, y in enumerate(self.y):
            plot_args = (self.x, y) + args
            if type(ax) is list:
                ax[i].plot(*plot_args, **kwargs)
            else:
                ax.plot(*plot_args, **kwargs)
        # See if the fig.show() comand is needed.
        if show:
            fig.show()
        return fig, ax

    def add_y(self, new_y):
        """
        Includes a new y in the signal.
        """
        new_y = np.array(new_y)
        if self.y[0].shape != new_y.shape:
            print('New Y shape', new_y.shape, ' does not match existing one ',
                  self.y[0].shape, ' nothing done.')
        else:
            self.y = np.vstack((self.y, new_y))

    # Import the functions from the function.py file to use as
    # NumpySignal methods.
    from .functions import (
        fft,
        fft_inverse,
        integral,
        integral_cumulative,
        cut,
        fit_linear,
        mean,
        set_mean_null,
        select_y,
        sort_by_x,
        autocorrelation,
        psd_autocorrelation,
        ptp,
        fit_curve,
        apply_function_y,
        apply_function_xy,
        apply_function_x,
        derivative,
        add_padding,
        savez,
        savetxt,
        equalize_energy,
        abs,
        get_power_rms,
        filter_butterworth,
        real,
        imag,
        sample,
        add_delay,
        extended_domain,
        propagate_wave,
        filter_butterworth_highpass
    )
