# Standard library imports
from functools import partial
#from concurrent.futures import ProcessPoolExecutor,ThreadPoolExecutor
from multiprocessing import Pool

# Third party imports
from tqdm import tqdm
import pandas as pd
import matplotlib.pyplot as plt

# Internal Package imports
from .signal.functions import fft
from .spectral_semblance import semblance
from awlat import settings
import awlat as aw

def _depth_function_call_by_process(function, args, kwargs, depth):
    """
    Callback function used to enable multiprocess - function version.
    Defined outside the class to avoid the copy of all the depths data
    to all processes.
    """
    return function(depth, *args, **kwargs)


def _depth_method_call_by_process(method_name, args, kwargs, depth):
    """
    Callback function used to enable multiprocess - method version.
    Defined outside the class to avoid the copy of all the depths data
    to all processes.
    """
    depthmethod = getattr(depth, method_name)
    return depthmethod(*args, **kwargs)


class WellLog:
    """
    Class to deal with a well data.
    Some helper methods are included for common tasks.
    A general "run for all depths" can be called by
    WellLog.depthmethod(args) .
    """

    __initialized = False

    def __init__(self, tool, depths):
        self.tool = tool
        self.well_id = None  # (optional)
        self.depths = depths
        self.__initialized = True

    @classmethod
    def new(cls, x, *args, **kwargs):
        """
        Creates a new instance of WellLog or a child class.
        To be used in the "not in-place" methods to ensure that the
        result is from the same class as the object used to create it.
        """
        return cls(x, *args, **kwargs)

    def __getstate__(self):
        #self_dict = self.__dict__.copy()
        # print(self_dict)
        #del self_dict['pool']
        return self.__dict__

    def __setstate__(self, d):
        self.__dict__ = d

    def __getattr__(self, name):
        """
        Passes the method call to the depth class and, if found, runs it
        for all depths.
        Returns new WellLog instance if depths returns new depths
        otherwise returns a list with the results.
        """
        def wrapper(*args, **kwargs):
            workers = kwargs.pop('workers', False)
            if workers:
                print('Warning: using multiprocess does not produce persistent'
                      ' changes in instance internal state. Make sure to use '
                      'this operation output only.')
                with Pool(processes=workers) as pool:
                    result = pool.map(
                        partial(_depth_method_call_by_process,
                                name, args, kwargs), tqdm(self.depths))
            else:
                result = []
                for i, depth in enumerate(tqdm(self.depths)):
                    method = getattr(depth, name)
                    result.append(method(*args, **kwargs))
            if [isinstance(result[i], type(self.depths[i]))
                    for i in range(len(result))] == [True]*len(result):
                return self.new(self.tool, result)
            else:
                return result
        return wrapper

    def run_for_all_depths(self, function, *args, workers=1, **kwargs):
        if workers:
            print('Warning: using multiprocess does not produce persistent '
                  'changes in instance internal state. Make sure to use this '
                  'operation output only.')
            with Pool(processes=workers) as pool:
                result = pool.map(partial(
                    _depth_function_call_by_process, function, args, kwargs),
                    tqdm(self.depths))
        else:
            result = []
            for i, depth in enumerate(tqdm(self.depths)):
                result.append(function(depth, *args, **kwargs))
        if [isinstance(result[i], type(self.depths[i]))
                for i in range(len(result))] == [True]*len(result):
            return self.new(self.tool, result)
        else:
            return result

    def __setattr__(self, name, value):
        """
        To facilitate populating depths attributes, if a list is given,
        matches it to the depths attributes.
        """
        if self.__initialized and name not in self.__dict__.keys():
            for i, attr_value in enumerate(value):
                setattr(self.depths[i], name, attr_value)
        else:
            object.__setattr__(self, name, value)

    @property
    def par_table(self):
        """
        Compiles the depths' data.
        Returns a pandas' data frame.
        """
        depth_pars = []
        for depth in self.depths:
            depth_pars.append(depth.pars)
        return pd.DataFrame(depth_pars)

    def update_par_table(self, table):
        """
        Loads a Dataframe with parameters values and overwrites the
        depths ones.
        """
        for i, row in enumerate(table.iterrows()):
            row_dict = row[1].to_dict()
            intersection = set(row_dict).intersection(set(self.depths[i].pars))
            self.depths[i].pars.update((k, row_dict[k]) for k in intersection)

    def plot_waveforms(
        self,
        get_wf: aw.signal.NumpySignal,
        *args: "matplotlib args",
        range: "depth range to plot" = None,
        ax: "use an existing axis to plot" = None,
        **kwargs: "matplotlib kwargs"
    ) -> "figure,axis":
        """
        Plot a waveform from a depth (given by the get_wf functions) as
        a function of depth (usual well logging style).

        :param get_wf: function to extract a NumpySignal from the depth
        """
        if ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111)
        else:
            fig = ax.figure   
        # Inverting the y axis direction to match the well depth natural
        # one.
        plt.gca().invert_yaxis()
        for depth in self.depths:
            if range is None or (
                    depth.depth >= range[0] and depth.depth < range[1]):
                # FIXME maybe do something more optimized...
                waveform = get_wf(depth) + depth.depth
                plot_args = (waveform.x, waveform.y[0]) + args
                ax.plot(*plot_args, **kwargs)
        return fig, ax


"""
Factories...
"""


def create_well(df, logs, tool):
    """
    Create a Well (monopole) from pandas dataframe metadata.
    """
    depths = []
    for i, row in enumerate(df.iterrows()):
        depths.append(settings.DepthMonopole(
            logs[i], tool, **row[1].to_dict()))
    return WellLog(tool, depths)


def create_well_dipole(df, logs, tool):
    """
    Create a Well (dipole) from pandas dataframe metadata.
    """
    depths = []
    for i, row in enumerate(df.iterrows()):
        depths.append(settings.DepthDipole(logs[i], tool, **row[1].to_dict()))
    return WellLog(tool, depths)


def load_txt_well(files):
    # FIXME not implemented...
    pass
