# Standard library imports
from math import pi

# Third party imports
#import dlisio
import numpy as np


# Internal Package imports
from awlat import AcousticTool
from awlat import settings

def unit_factor_to_SI(unit):
    """
    Gets the factor to transform a distance unit to SI in the expected
    format of "0.1 in" or just "in".
    """
    unit_to_SI = {'m': 1.,
                  'in': 0.0254,
                  'ft': 0.3048,
                  'rad': 1.,
                  'deg': pi/180,
                  's': 1.,
                  'us': 1e-6,
                  'ms': 1e-3}

    splitted = unit.split(' ')
    if len(splitted) == 1:  # no unit factor
        return unit_to_SI[splitted[0]]
    else:
        return float(splitted[0]) * unit_to_SI[splitted[1]]


# DLIS file Keywords and they analogous in the AWLAT
known_relations = {
    'HDAR': 'hole_diameter',
    'INCL': 'hole_inclination',
    'P1AZ': 'phi_tool',
    'WF_XDP_IN': 'XX',
    'WF_XDP_OFF': 'XY',  # FIXME Verify!
    'WF_YDP_IN': 'YY',
    'WF_YDP_OFF': 'YX',
    'TDEP': 'depth',
}

# Inverse known_relations dictionary.
know_relations_inverted = {value: key for key,
                           value in known_relations.items()}


def get_time_axis(channel, index=None):
    """
    Parses a dlisio channel and returns a time axis.
    """
    if index is None:
        for i, axis in enumerate(channel.axis):
            if 'TIME' in str(axis) or 'time' in str(axis):
                index = i
    if index is None:
        raise ValueError('No time related axis found. Try \
        specifying the axis index manually with the index keyword.')
    # getting the time axis lenght
    N = channel.dimension[index]
    spacing = axis.spacing * unit_factor_to_SI(axis.attic['SPACING'].units)
    # FIXME hardcoding the 0 index in the axis coordinates
    coordinate = (axis.coordinates[0]
                  * unit_factor_to_SI(axis.attic['COORDINATES'].units))
    return np.arange(coordinate, spacing*N+coordinate, spacing)


def get_dipole_shots(components_channels):
    """
    Parses the channels to get the dipole shots.
    """
    #depth_curve = depth_channel.curves()
    CC_curves = [channel.curves() for channel in components_channels]
    # Using the first channel to get the time axis.. assuming they are
    # all the same
    time = get_time_axis(components_channels[0])
    shots = []
    N = CC_curves[0].shape[0]
    for i in range(N):
        CC_shots = [settings.ShotMonopole(time, curve[i, 0, :, :])
                    for curve in CC_curves]
        shots.append(settings.ShotDipole(*CC_shots))
    return shots


def get_depths(frame, shots, tool):
    """
    FIXME
    """
    depth_channel = get_depth_channel(frame)
    depth_curve = depth_channel.curves()
    depth_spacing = unit_factor_to_SI(depth_channel.units)
    depths = []
    for i, depth in enumerate(depth_curve):
        depth_in_meter = depth*abs(depth_spacing)
        depths.append(
            settings.DepthDipole(shots[i], tool, depth=depth_in_meter)
            )
    return depths


def add_channel_to_well(well, ch_name, frame):
    """
    Adds a channel data to the depths.
    """
    try:
        ch_name_dlis = know_relations_inverted[ch_name]
    except KeyError:
        ch_name_dlis = ch_name
    for ch in frame.channels:
        if ch_name_dlis in str(ch):
            break
    curve = ch.curves()
    for i, depth in enumerate(well.depths):
        depth.pars.update({ch_name: curve[i]})


def get_depth_channel(frame):
    for ch in frame.channels:
        if 'TDEP' in str(ch):
            return ch
    return None


def get_tool(tool, channel_index=0):
    npole = 1
    diameter = None
    ch = tool.channels[channel_index]
    for i, axis in enumerate(ch.axis):
        if 'SENSOR_OFFSET' in str(axis):
            number_of_receivers = ch.dimension[i]
            receivers_spacing = (axis.spacing
                                 * unit_factor_to_SI(
                                     axis.attic['SPACING'].units))
            receivers_coordinate = (axis.coordinates[0]
                                    * unit_factor_to_SI(
                                        axis.attic['COORDINATES'].units))
        elif 'SOURCE_OFFSET' in str(axis):
            source_coordinate = (axis.coordinates[0]
                                 * unit_factor_to_SI(
                                     axis.attic['COORDINATES'].units))
    first_receiver_distance_from_source = (source_coordinate
                                           - receivers_coordinate)
    receivers_spacing = abs(receivers_spacing)
    return AcousticTool(npole,  diameter,
                        number_of_receivers, receivers_spacing,
                        first_receiver_distance_from_source)
