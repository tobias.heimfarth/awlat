import sys
from cmath import pi, sqrt, exp, sin, cos, sqrt
from functools import partial

import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import butter, lfilter
from scipy.signal import freqz
from tqdm import tqdm

from .grid import NumpyGrid
from .signal import NumpySignal


def plot_semblance(npg, show=False, **kargs):
    # For now just coping the NumpyGrid plot style.
    print(**kargs)
    fig, ax = npg.plot(show=False, **kargs)
    # Renaming axis.
    fig.suptitle('Semblance')
    ax.set_xlabel('Frequency')
    ax.set_ylabel('Slowness')
    if show:
        plt.show()
    return fig, ax


def rho(X_f, f, s, d):
    """
    Semblance "rho".
    """
    N = len(X_f.y)
    num = 0
    den = 0
    print(f)
    for i, X in enumerate(X_f(f)):
        num += np.conjugate(X)*exp(-1j*2*pi*f*s*d[i])
        den += abs(X)**2
    return abs(abs(num)/sqrt(N*den))


def rho_prenorm(A_f, f, s, d, norm):
    """ 
    Semblance "rho" not computing the normalization that should be
    given as an argument. Computing the normalization factors beforehand
    improves speed.
    """
    numerador = 0
    a = 1j*2*pi*f*s
    for i, Ai in enumerate(A_f):
        numerador += Ai*exp(a*d[i])
    return abs(numerador/norm)


def rho_norm(X_f, f):
    """
    Semblance "rho"'s normalization factors. To be used in
    conjunction with rho_prenorm function.
    """
    X_filtered = X_f(f)
    N = len(X_f.y)
    d = 0
    for X in X_filtered:
        d += abs(X)**2
    return sqrt(N*d)


def rho_prenorm_vectorized(signal_f, s, d, norm):
    """
    FIXME docs...
    """
    numerator = np.zeros(signal_f.length)
    a = signal_f.x*1j*2*pi*s
    for i, y in enumerate(signal_f.y):
        numerator = numerator + y*np.exp(a*d[i])
    return np.abs(numerator/norm)


def rho_norm_vectorized(signal_f):
    """
    FIXME docs...
    """
    N = signal_f.y_len
    d = np.zeros(signal_f.length)
    for y in signal_f.y:
        d += np.abs(y)**2
    return np.sqrt(N*d)


def semblance(
        data_f,
        detectors_positions,
        slow_min=0,
        slow_max=10e-4,
        ds=1e-5,
        freq_min=0,
        freq_max=10e3,
        df=10):
    """ Computes the semblance array (numpy array). """
    # Creating an uniform grid
    freq, slow = np.mgrid[freq_min:freq_max:df, slow_min:slow_max:ds]
    # Calculating the normalization factors (denominator)
    #  as they are the same in multiple points (improve speed)
    norm = []
    for f in freq[:, 0]:
        norm.append(rho_norm(data_f, f))
    # Computing rho matrix
    semblance = []
    for i, f_line in enumerate(freq):
        line = []
        # Obtaining the spectrum amplitudes at the grid frequency
        A_f = data_f(f_line[0])
        for j, f in enumerate(f_line):
            line.append(rho_prenorm(A_f, f, slow[i][j],
                                    detectors_positions, norm[i]))
        # print(f)
        semblance.append(line)
    return NumpyGrid(freq, slow, np.array(semblance))


def semblance_vectorized(
        data_f,
        detectors_positions,
        slow_min=0,
        slow_max=10e-4,
        ds=1e-5,
        freq_min=0,
        freq_max=10e3,
        df=10):
    """ Computes the semblance array (numpy array). """
    # Creating an uniform grid
    freq, slow = np.mgrid[freq_min:freq_max:df, slow_min:slow_max:ds]
    # Sampling the signal at the required frequencies
    fs = freq[:, 0]
    ss = slow[0]
    sampled_data_f = data_f.sample(fs)
    # Calculating the normalization factors (denominator)
    #  as they are the same in multiple points (improve speed)
    norm = rho_norm_vectorized(sampled_data_f)
    # Computing rho matrix
    semblance = []
    for s in ss:
        semblance.append(rho_prenorm_vectorized(
            sampled_data_f, s, detectors_positions, norm))
    semblance = np.array(semblance).transpose()
    return NumpyGrid(freq, slow, np.array(semblance))