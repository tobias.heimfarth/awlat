def find_closest_value_index(values, reference):
    '''
    Simple function to get the index of the closest value to a reference
    in a given list.
    '''
    if not values:
        return None
    diffs = [abs(v - reference) for v in values]
    d_min = min(diffs)
    return diffs.index(d_min)
