# -*- coding: utf-8 -*-
"""
Created on 22/06/2013

@author: tobiash
"""
import os.path
import numpy as np
import matplotlib.pyplot as plt

class NumpyGrid:
    '''
    Deals with two variable functions.
    Takes as input numpy arrays in the grid format (shape = (n,m)).
    '''
    def __init__(self,x,y,z):
        self.x = x
        self.y = y
        self.z = z
        self._dx = None
        self._dy = None
        self._xlim = None
        self._ylim = None

    @classmethod
    def new(cls,*args):
        '''
        Creates a new instance of NumpyGrid or a child class.
        To be used in the "not in-place" methods to ensure that the
        result is from the same class as the object used to create it.
        Example: If a is from the A class, a child of GridSignal,
        then func(a) also is from class A.
        '''
        return cls(*args)
    
    def __add__(self,value):
        # Adding to a null value
        if not value:
            return self.new(self.x,self.y,self.z)
        # FIXME Adding to a NumpyGrid without cheking if x and y match.
        else:
            return self.new(self.x,self.y,self.z+value.z)

    def __mul__(self,value):
        return self.new(self.x,self.y,self.z*value)
    
    def __call__(self,x,y):
        '''
        Returns the interpolated z value from a x and y value.
        FIXME: no guardrails.
        '''
        # Getting low corner of the grid rectangle containing the point
        xi_low = int((x-self.x[0,0])/self.dx)
        yi_low = int((y-self.y[0,0])/self.dy)
        # Making sure the indexes dos not fall of the range.
        if xi_low == self.x.shape[0]-1: xi_low += -1
        if yi_low == self.x.shape[1]-1: yi_low += -1
        # Getting the values of this low corner
        x_low = self.x[xi_low,yi_low]
        y_low = self.y[xi_low,yi_low]
        z_low = self.z[xi_low,yi_low]
        # Partial heights
        dZx = self.z[xi_low+1,yi_low] - self.z[xi_low,yi_low]
        dZy = self.z[xi_low,yi_low+1] - self.z[xi_low,yi_low]
        # Z change
        dZ = dZx*(x-x_low)/self.dx + dZy*(y-y_low)/self.dy
        return z_low + dZ
    
    def plot(self,
            title='',
            xlabel='',
            ylabel='',
            show=False,
            **kargs):
        ''' A pre configured matplotlib semplance plot, for quick plotting.
        '''
        fig = plt.figure()
        ax = fig.gca()
        ax.pcolor(self.x, self.y, self.z, cmap='jet',shading='auto',**kargs)
        ax.axis([self.x.min(), self.x.max(), self.y.min(), self.y.max()])
        fig.suptitle(title)
        ax.set_xlabel(xlabel)
        ax.set_ylabel(ylabel)
        #Depending on the environment being used this plt.show() is a problem.
        if show: plt.show()
        return fig,ax
 
    def savez(self,file_name):
        print('Saving compressed arrays to '+file_name+' .')
        np.savez(file_name,self.x,self.y,self.z)

    def savetxt(self,filename,separator = ' ',xformat='{:.5e}',yformat='{:.5e}'): #,zformat='{:.5e}'):
        '''
            #FIXME quick veusz compatible hack. Do something better.
        '''
        filename = filename.split('.')[0]
        savefile = open(filename+'_x.txt','w')
        for x in self.x:
            line = xformat.format(x[0])
            savefile.write(line+'\n')
        savefile.close()
        savefile = open(filename+'_y.txt','w')
        for y in self.y.T:
            line = yformat.format(y[0])
            savefile.write(line+'\n')
        savefile.close()
        np.savetxt(filename+'_z.txt',np.flipud(self.z.T))
        
    
    @property
    def dx(self):
        if self._dx is None:
            self._dx = self.x[1][0] - self.x[0][0]
        return self._dx

    @property
    def dy(self):
        if self._dy is None:
            self._dy = self.y[0][1] - self.y[0][0]
        return self._dy

    @property
    def xlim(self):
        if self._xlim is None:
            self._xlim = [self.x[0][0], self.x[-1][0]]
        return self._xlim

    @property
    def ylim(self):
        if self._ylim is None:
            self._ylim = [self.y[0][0], self.y[0][-1]]
        return self._ylim
    
    from .functions import filter_gaussian,find_max_curve,filter_gaussian_manual


def create_numpygrid_from_function(f,x_min,x_max,dx,y_min,y_max,dy):
    x,y = np.mgrid[x_min:x_max:dx,y_min:y_max:dy]
    f = np.vectorize(f)
    return NumpyGrid(x,y,f(x,y))

