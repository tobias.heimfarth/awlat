# Standard library imports
from math import sqrt, pi, exp

# Third party imports
import numpy as np
from tqdm import tqdm
from scipy.ndimage import gaussian_filter1d as scipy_gaussian_filter

# Internal Package imports
from ..signal.numpy_signal import NumpySignal
from ..signal.functions import (cut, apply_function_xy, integral, max_parabola,
                                local_max_index, get_closest_maximum)
from ._auxiliary import find_closest_value_index


def _gauss(x, mu, sigma):
    """ Simple gaussian function
    """
    return 1/sqrt(2*pi*sigma**2)*exp(-(x-mu)**2/(2*sigma**2))


def filter_gaussian_manual(npg, ws_sigma, **kargs):
    """ 
    Runs a soothering gaussian filter in nearby frequencies to reduce
    noise in the semblance.
    ws_sigma: the filtering weight gaussian sigma in Hz, ws_sigma = 200
    is a typical value.
    """
    def gauss_partial(x, y): return y*_gauss(x, f, ws_sigma)
    z_filtered = []
    for i in range(len(npg.y[1])):
        line = []
        sig = NumpySignal(npg.x[:, i], npg.z[:, i])
        f_line = []
        for f in npg.x[:, i]:
            a = cut(sig, f-ws_sigma, f+ws_sigma)
            a = apply_function_xy(a, gauss_partial)
            a = integral(a)
            #a = sig.cut(f-ws_sigma,f+ws_sigma)
            #   .apply_function_xy(lambda x,y: y*gauss(x,f,ws_sigma))
            #   .integral()
            f_line.append(abs(a[0]))
        z_filtered.append(f_line)
    return npg.new(npg.x, npg.y, np.array(z_filtered).transpose())


def filter_gaussian(npg, ws_sigma, **kw_args):
    new_z = scipy_gaussian_filter(npg.z, sqrt(ws_sigma), axis=0, truncate=1)
    return npg.new(npg.x, npg.y, np.array(new_z))


def find_max_curve(npg, diff_max=1, warnings=False):
    """
    !!! Messy code ahead !!! Refactor this function if it becomes
    important.
    Tries (very hard) to construct a continuous curve from local maximus
    in a given column of a 2D array. Starting form the global maximum
    and its neighbors, it uses a linear projection to both sides to
    choose find a close maximum and continue building the curve.
    diff_max : Sets the maximum difference between next point and the
    projected one. If not meet the maximum is not included in the curve.
    """
    # Parameters
    d = 3  # -> fit in between (xmax - d) and (xmax+d )
    # Lists to store the values
    curve_x = []
    curve_y = []
    # gets and unravel the index of the maximum of the semblance point
    max_arg = np.unravel_index(np.argmax(npg.z, axis=None), npg.z.shape)
    # print(max_arg)
    # Pre construct the x axis to use ahead (using 0,1,2 instead of the
    # slowness for simplicity)
    ys = np.arange(npg.z.shape[1])
    # Processing the maximum profile
    # Create signal from column to fit a parabola at its maximum
    sig = NumpySignal(ys, npg.z[max_arg[0]])
    # Record the coordinates of the maximum values
    max_x = max_arg[0]
    max_y, max_z = max_parabola(sig, d, sig.x[max_arg[1]])
    # Storing the maximum coordinates
    curve_x.append(max_arg[0])
    curve_y.append(max_y)
    # Copying the global maximum coordenates to point 1 variables.
    max_x_1, max_y_1, max_z_1 = max_x, max_y, max_z
    # Processing the near maximum profile to initiate the loop with the
    # derivative value
    # Choosing a viable neighboor
    if max_arg[0] < npg.z.shape[0]-1:
        max_x_2 = max_x_1 + 1
    else:
        max_x_2 = max_x_1 - 1
    # Creating the neighboor column signal
    sig_2 = NumpySignal(ys, npg.z[max_x_2])
    # Getting all maximuns
    max_ys_2 = local_max_index(sig_2)[0]
    # Choosing the closest one to the global
    max_y_2 = max_ys_2[find_closest_value_index(max_ys_2, max_y_1)]
    max_z_2 = sig_2(max_y_2)[0]
    # Calculating the derivative
    dy_dx_0 = (max_y_2 - max_y_1)/(max_x_2 - max_x_1)
    # Searching for the points on the left
    # Creating list of values in the left of the maximum
    xs_left = (list(range(max_arg[0])))
    xs_left.reverse()

    def run_branch(xs_branch):
        # Copying the global maximum coordenates to point 1 variables
        # (recopying inside the function).
        max_x_1, max_y_1, max_z_1 = max_x, max_y, max_z
        # Copying the derivative value to use inside the loop
        dy_dx = dy_dx_0
        for i in xs_branch:
            # Creating signal of the next column
            sig_2 = NumpySignal(ys, npg.z[i])
            max_x_2 = i
            # Calculating the linear projected next maximum (dx =1)
            projected_next_y = dy_dx * (max_x_2-max_x_1) + max_y_1
            # Calculating the closest maximum
            max_y_2, max_z_2 = get_closest_maximum(sig_2, projected_next_y)
            if max_y_2 is not None and max_z_2 is not None:
                # Checking if the slope is too high or low indicating
                # the end of the curve
                if (abs(projected_next_y - max_y_2) < diff_max
                        and abs(max_x_2 - max_x_1) < 10):
                    # Computing the updated derivative for the next
                    # point
                    dy_dx = (max_y_2 - max_y_1)/((max_x_2-max_x_1))
                    curve_x.append(i)
                    curve_y.append(max_y_2)
                    max_x_1, max_y_1, max_z_1 = max_x_2, max_y_2, max_z_2
                else:
                    if warnings:
                        print('Could not find next point. Assuming curve ends '
                             'at', i*npg.dx+npg.xlim[0], '.')
                    break
    run_branch(xs_left)
    # reversing the list to normal order
    curve_x.reverse()
    curve_y.reverse()
    # Searching for the points on the right of the global maximum
    max_x_1, max_y_1, max_z_1 = max_x, max_y, max_z
    xs_right = (list(range(max_arg[0]+1, npg.z.shape[0])))
    run_branch(xs_right)
    # Reconstruction x and y values from indexes
    curve_x = (np.array(curve_x) * npg.dx) + npg.xlim[0]
    curve_y = (np.array(curve_y) * npg.dy) + npg.ylim[0]
    return NumpySignal(curve_x, curve_y)
