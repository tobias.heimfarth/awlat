# Standard library imports
from copy import copy
from math import cos, pi, sqrt

# Third party imports
import numpy as np
from scipy.signal import butter, lfilter
from tqdm import tqdm

# Internal Package imports
from .signal import NumpySignal
from .grid import NumpyGrid


'''
FIXME code quality revision needed.
For now it is just a quick and dirty import form older stuff.
Not a central piece of code anyway.
'''


def stc(signals, tau_limits, dtau, slow_limits, ds, detectors_positions,
        window_width):
    '''
    Slowness time coerence.
    '''
    # Making a copy to avoid in place modification to the input
    signals = NumpySignal(signals.x, signals.y)
    # Creating the independent variables grid.
    tau, slow = np.mgrid[tau_limits[0]:tau_limits[1]:dtau,
                         slow_limits[0]:slow_limits[1]:ds]
    semblance = []
    for slow_line in slow[0]:
        line = rho_nondispersive(signals,
                                 slow_line,
                                 detectors_positions,
                                 tau[:, 0],
                                 window_width)
        semblance.append(line)
    semblance = np.transpose(np.array(semblance))
    # Finding the frequency specific maximums
    #max_index = np.unravel_index(z.argmax(),z.shape)
    # print(tau[max_index],slow[max_index],z[max_index])
    # slowness.append(slow[max_index])
    # semblances.append(z)
    return NumpyGrid(tau, slow, semblance)


def rho_nondispersive(signal, slowness, d, tau, window_width, pad=700):
    # Getting some signal usefull parameters
    detectors = signal.y_len
    # Time shifting
    # Padding the signal to avoid running out of samples (t>tmax)
    padded_signal = signal.add_padding(pad)
    # List to store the time shifted ys
    shifted_ys = []
    for i in range(detectors):
        delta_t = slowness*(d[i]-d[0])
        shifted_single_y = []
        for j, t in enumerate(signal.x):
            # print(t+delta_t,t,delta_t)
            y = padded_signal.select_y(i)(t+delta_t)[0]
            shifted_single_y.append(y)
        shifted_ys.append(shifted_single_y)
    R = NumpySignal(signal.x, shifted_ys)
    R_d_squared = R**2
    den = sum(R_d_squared.integral())
    R_n_squared = (NumpySignal(R.x, R.y.sum(axis=0)))**2
    semblance = []
    for tau_ in tau:
        # Denominatore normalized per tau (uncoment).
        # den = sum(R_d_squared.cut(tau_,tau_+window_width).integral())
        # Numerador
        num = R_n_squared.cut(tau_, tau_+window_width).integral()[0]
        semblance.append(1/detectors*num/den)
    return semblance


def slownessProfile(s, N, f, d):
    a = 0
    #den = 0
    for n in range(N+1)[1:]:
        for k in range(N+1)[1:]:
            a += cos(2*pi*f*d*(n-k)*s)
            #den += cos(2*pi*f*d*(n-k))
    a = abs(a)
    return sqrt(a)/N


def slownessProfileNorm(s, s0, N, f, d):
    a = 0
    den = 0
    for n in range(N+1)[1:]:
        for k in range(n+1)[1:]:
            a += cos(2*pi*f*d*(n-k)*(s-s0))
            den += cos(s0*2*pi*f*d*(n-k))**2
    return abs(sqrt(a**2/den))*(1/N)
