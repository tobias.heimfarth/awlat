# Awlat

Awlat is an Acoustic Wave Logging Analysis Tools package.

## The code is structure in the following way:

* Tier 1:
    * "signal" and "grid" modules provides the basic classes (NumpySignal and NumpyGrid) for dealing with signals (functions in time) and 2D maps and are used, along with numpy arrays, as the basic objects to store and manipulate the log data.
    * Both contain general use methods not specific to acoustic logs.

* Tier 2:
    * The second group of components are the specialized functions for acoustic logging like computing the semblance, stc, etc. This functions takes as input anything form Tier 1.

* Tier 3:
    * In this tier the data is structured from a "shot" to a complete well, in a way to automatize the well processing.
    * The main classes are:
        * AcousticTool (or tool-like) - tool data like diameter, spacing, etc...
        * Shot (or shot-like) - Contains a single data shot, with the pressure vs time for all receivers and multiple components (XX,YY,...)
        * Depth (or depth-like) - Contains the data from the shots with information from a given depth and all the methods to analyze it.
        * WellLog (or well-like) - Group of depths with helpers to loop over all depths and compile the data.

FIXME (write something usefull...)
