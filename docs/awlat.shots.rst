awlat.shots package
===================

Submodules
----------

awlat.shots.dipole module
-------------------------

.. automodule:: awlat.shots.dipole
   :members:
   :undoc-members:
   :show-inheritance:

awlat.shots.factories module
----------------------------

.. automodule:: awlat.shots.factories
   :members:
   :undoc-members:
   :show-inheritance:

awlat.shots.monopole module
---------------------------

.. automodule:: awlat.shots.monopole
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: awlat.shots
   :members:
   :undoc-members:
   :show-inheritance:
