awlat.synthetic package
=======================

Submodules
----------

awlat.synthetic.dipole module
-----------------------------

.. automodule:: awlat.synthetic.dipole
   :members:
   :undoc-members:
   :show-inheritance:

awlat.synthetic.direct\_waves module
------------------------------------

.. automodule:: awlat.synthetic.direct_waves
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: awlat.synthetic
   :members:
   :undoc-members:
   :show-inheritance:
