tests package
=============

Submodules
----------

tests.context module
--------------------

.. automodule:: tests.context
   :members:
   :undoc-members:
   :show-inheritance:

tests.memory\_profiling module
------------------------------

.. automodule:: tests.memory_profiling
   :members:
   :undoc-members:
   :show-inheritance:

tests.profiling module
----------------------

.. automodule:: tests.profiling
   :members:
   :undoc-members:
   :show-inheritance:

tests.test\_depth module
------------------------

.. automodule:: tests.test_depth
   :members:
   :undoc-members:
   :show-inheritance:

tests.test\_signal module
-------------------------

.. automodule:: tests.test_signal
   :members:
   :undoc-members:
   :show-inheritance:

tests.test\_well module
-----------------------

.. automodule:: tests.test_well
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: tests
   :members:
   :undoc-members:
   :show-inheritance:
