from .context import awlat as aw
import numpy as np
from math import pi
import pytest


x = np.arange(0,10,0.01)
y = 20*np.sin(x*10)

sample_signal = aw.signal.NumpySignal(x,y)

def test_NumpySignal_creation():
    ## creating from single y.
    sample_signal = aw.signal.NumpySignal(x,y)
    np.testing.assert_array_equal(sample_signal.x,np.array(x))
    np.testing.assert_array_equal(sample_signal.y,np.array([y]))
    ## creating from single [y].
    sample_signal = aw.signal.NumpySignal(x,[y])
    np.testing.assert_array_equal(sample_signal.x,np.array(x))
    np.testing.assert_array_equal(sample_signal.y,np.array([y]))
    ## creating from multiple individual ys.
    sample_signal = aw.signal.NumpySignal(x,y,y)
    np.testing.assert_array_equal(sample_signal.x,np.array(x))
    np.testing.assert_array_equal(sample_signal.y,np.array([y,y]))
    ## creating from a list of ys.
    sample_signal = aw.signal.NumpySignal(x,[y,y])
    np.testing.assert_array_equal(sample_signal.x,np.array(x))
    np.testing.assert_array_equal(sample_signal.y,np.array([y,y]))

def test_nps_fft():
    fft = sample_signal.fft()
    # fft at the main frequency corresponds to the amplitude.
    assert fft.abs()(1.6)[0] == pytest.approx(20.,abs=0.2)
    # away from the main frequency must be approximately zero.
    assert fft.abs()(20/(2*pi))[0] == pytest.approx(0.0,abs=0.2)
    
def test_plot():
    sample_signal.plot(show=True)

def test_stack():
    x1 = [1,2,3,4,5,6]
    y11 = [3,4,5,6,7,8]
    y12 = [1,2,1,0,0,1]
    s1 = aw.signal.NumpySignal(x1,[y11,y12])
    x2 = [0,1,2,3,4,5]
    y2 = [-3,-4,-5,-6,-7,-8]
    s2 = aw.signal.NumpySignal(x2,y2)
    sr = aw.signal.stack([s1,s2],x1)
    np.testing.assert_array_equal(sr.x,x1)
    np.testing.assert_array_almost_equal(sr.y,[y11,y12,[-4,-5,-6,-7,-8,0]])