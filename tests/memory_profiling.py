from memory_profiler import profile
from context import awlat as aw
from context import awlat_dir


@profile
def test_simple_semblance():
    log = aw.shots.load_shot_monopole(
        awlat_dir+'/data/fast_experimental/FastDataP025.txt')
    log_f = log.fft()
    d = [i*0.2 for i in range(8)]
    semb = aw.spectral_semblance.semblance_vectorized(log_f, d)
    semb1 = semb.filter_gaussian(200)
    #semb2 = semb.filter_gaussian_manual(200)
    # semb1.plot(show=True)
    # semb2.plot(show=True)

if __name__ == "__main__":
     test_simple_semblance()