# -*- coding: utf-8 -*-
from .context import awlat as aw
import numpy as np
from math import pi
import pytest


tool = aw.AcousticTool(1,10,8,0.2,3.)

def test_tool_receivers_positions():
    assert tool.receivers_positions == [3 + i*0.2 for i in range(8)]

def test_tool_midpoints():
    print(tool.receivers_midpoints())
