'''
Provides the local filesystem context to avoid import problems.
'''
import os
import sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import awlat


awlat_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

