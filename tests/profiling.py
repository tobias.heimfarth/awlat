import cProfile
import pstats
import io
from context import awlat as aw
from context import awlat_dir as aw_dir
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import dlisio


def profile(fnc):
    """
    A decorator that uses cProfile to profile a function.
    Used to measure the time a patch of code takes to run.
    """
    def inner(*args, **kwargs):

        pr = cProfile.Profile()
        pr.enable()
        retval = fnc(*args, **kwargs)
        pr.disable()
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())
        return retval

    return inner



@profile
def test_simple_semblance():
    log = aw.shots.load_shot_monopole(
        aw_dir+'/tests/fast_experimental/FastDataP025.txt')
    log_f = log.fft()
    d = [i*0.2 for i in range(8)]
    semb = aw.spectral_semblance.semblance_vectorized(log_f, d)
    semb1 = semb.filter_gaussian(200)
    #semb2 = semb.filter_gaussian_manual(200)
    # semb1.plot(show=True)
    # semb2.plot(show=True)


@profile
def test_find_max_curve():
    log = aw.shots.load_shot_monopole(
        aw_dir+'/tests/fast_experimental/FastDataP025.txt')
    d = [0.2*i for i in range(8)]
    log_f = log.fft()
    semb = aw.spectral_semblance.semblance(log_f, d)
    for i in range(10):
        semb.find_max_curve()


@profile
def test_rotate_dipole_components():
    sm = aw.shots.load_shot_monopole(
        aw_dir+'/tests/fast_experimental/FastDataP025.txt')
    tool = aw.AcousticTool(1, None, 8, 0.2, 0)
    sd = aw.synthetic.reflection_shot_S_wave(sm,10,0.1,1600,0.2,(1,0.1),tool)
    for i in range(1000):
        aw.rotate_components(sd,0.1)


def stack_by_receiver():
    files = dlisio.load(aw_dir+
        '/awlat/special/imaging/examples/sample_data/P1_MSIP_BARS_V3_30m.dlis')
    lf = files[0]
    tool = aw.data_import.dlis.get_tool(lf.object('TOOL','MAPC'))
    shots = aw.data_import.dlis.get_dipole_shots(
        [lf.object('CHANNEL','WF_XDP_IN'),
        lf.object('CHANNEL','WF_XDP_OFF'),
        lf.object('CHANNEL','WF_YDP_OFF'),
        lf.object('CHANNEL','WF_YDP_IN')])
    frame = lf.frames[0]
    depths = aw.data_import.dlis.get_depths(frame,shots,tool)
    # For now awlat expects the depths to be in increasing order, not the case in this DLIS file, so reverting it.
    depths.reverse()
    @profile
    def run_stack():
        d = aw.depths.stack_by_receiver_fixed_position(depths)
    run_stack()




if __name__ == "__main__":
    # test_average_depth_stacking()
    # test_simple_semblance()
    # test_spectre()
    # test_H_equivtool()
    #test_find_max_curve()
    #test_rotate_dipole_components()
    stack_by_receiver()
