from .context import awlat as aw
import numpy as np
import pytest


x = np.arange(0,10,0.01)
y = 20*np.sin(x*10)
shot = aw.shots.monopole.ShotMonopole(x,y)
dipole_shot = aw.shots.dipole.ShotDipole(shot,shot,shot,shot)
tool = aw.tool.AcousticTool(1,1,10,0.1,10)

def test_depth_pars_copy():
    pars = {'a':1,'b':2}
    depth = aw.depths.dipole.DepthDipoleSemblance(dipole_shot,tool,**pars)
    new_depth = depth.new(dipole_shot,tool,**depth.pars)
    assert depth.pars == new_depth.pars
    